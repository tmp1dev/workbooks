#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <fstream>

// ref.
// https://stackoverflow.com/a/10195497
//

bool move_flag, raw_flag;
int argi[2] = { -1, -1 };

void check_options(int, char**);


int main(int argc, char *argv[])
{
    char buffer[BUFSIZ];
    size_t size;
    
    check_options(argc, argv);   
    if (argi[0] < 0) {
        puts("usage: copy [-M, --move | -R, --raw] source_file target_file");
        exit(0);       
    }
    else if (argi[1] < 0) {
        puts("target filename is required");
        exit(0);       
    }
    
    // raw option
    // https://stackoverflow.com/q/10195343
    //
    if (raw_flag) {
        FILE *source = fopen(argv[argi[0]], "rb");
        FILE *target = fopen(argv[argi[1]], "wb");
        if (source == NULL || target == NULL) {
            puts("No such file or directory");
            exit(EXIT_FAILURE);
        }
        
        while ( (size = fread(buffer, 1, BUFSIZ, source)) ) {
            fwrite(buffer, 1, size, target);
        }
        
        fclose(source);
        fclose(target);
    }
    else {
        std::ifstream source(argv[argi[0]], std::ios::binary);
        std::ofstream target(argv[argi[1]], std::ios::binary);
        if (source == NULL || target == NULL) {
            puts("No such file or directory");
            exit(EXIT_FAILURE);
        }      
        
        target << source.rdbuf();
    }

    // move/rename option
    //
    if (move_flag) {
        if ( remove(argv[argi[0]]) != 0 ) {
            puts("No such file or directory");
            exit(EXIT_FAILURE);
        }
    }
}

void check_options(int argc, char **argv)
{
    for (int i=1; i < argc; i++) {
        char *param = *(argv+i);
        
        if ( strcmp(param, "-M") == 0 || strcmp(param, "--move") == 0 ) {
            move_flag = true;            
            continue;
        }
        if ( strcmp(param, "-R") == 0 || strcmp(param, "--raw") == 0 ) {
            raw_flag = true;
            continue;
        }
        if (argi[0] < 0) {
            argi[0] = i;
        }
        else {
            argi[1] = i;
        }

    }
}
